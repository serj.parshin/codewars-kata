package PrizeDraw;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Data
public class Lottery {
    private String[] participants;
    private Integer[] weights;
    private Map<Integer, String> candidates = new HashMap<>();
    private ArrayList<Integer> ranks = new ArrayList<>();
    private String winner;
    private int n;
    private boolean counted;

    public Lottery(String st, Integer[] we, int n){
        this.participants = st.split("\\W");
        this.weights = we;
        this.n = n;
        this.counted = false;
        this.candidates.clear();
        this.ranks.clear();
    }

    public boolean isCounted() {
        return counted;
    }

    private int getCharsRank(String name){
        int sum = 0;
        for (char ch:name.toCharArray()) {
            sum += (Character.getNumericValue(ch) - 9);
        }
        return sum;
    }

    public Integer countRank(String name, Integer weight){
        return (getCharsRank(name) + name.length()) * weight;
    }

    private String countAllRanks(){
        for (int i = 0; i < participants.length; i++) {
            String name = participants[i];
            Integer rank = countRank(name, weights[i]);
            ranks.add(rank);
            candidates.put(rank, name);
        }
        Collections.sort(ranks);
        Collections.reverse(ranks);
        return setWinner(ranks.get(n-1));
    }

    private final String setWinner(Integer rank){
        return candidates.get(rank);
    }

    public String getWinner(){
        if (participants.length == 0) {
            return "No participants";
        } else {
            winner = countAllRanks();
            return winner;
        }
    }
}
