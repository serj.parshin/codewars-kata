package PrizeDraw;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

class Rank {

    static class Candidate implements Comparable<Candidate>{
        private String name;
        private Integer rank;

        Candidate(String name, Integer weight){
            this.name = name;
            this.rank = countRank(name, weight);
        }

        public String getName() {
            return name;
        }

        public Integer getRank() {
            return rank;
        }

        @Override
        public String toString() {
            return "Candidate{" +
                    "name='" + name + '\'' +
                    ", rank=" + rank +
                    '}';
        }

        private static Integer countRank(String name, Integer weight){
            return (getCharsRank(name) + name.length()) * weight;
        }

        private static int getCharsRank(String name){
            int sum = 0;
            for (char ch:name.toCharArray()) {
                sum += (Character.getNumericValue(ch) - 9);
            }
            return sum;
        }

        @Override
        public int compareTo(Candidate candidate) {
            if(this.rank == candidate.getRank())
                if(this.name.compareTo(candidate.getName()) < 0) return -1;
                else return 1;
            else if (this.rank > candidate.getRank())
                return -1;
            else
                return 0;
        }
    }

    public static String nthRank(String st, Integer[] we, int n) {
        if(st.isEmpty()) return "No participants";
        if(we.length < n) return "Not enough participants";

        List<String> members = Arrays.asList(st.split("\\W"));
        System.out.println(members);

        List<Candidate> candidates = new ArrayList<>();
        for (int i = 0; i < members.size(); i++) {
            candidates.add(new Candidate(members.get(i), we[i]));
        }
        System.out.println(candidates);

        List<Integer> ranks = new ArrayList<>();
        candidates.forEach(candidate -> {
            ranks.add(candidate.getRank());
        });
        ranks.sort(Comparator.reverseOrder());
        System.out.println(ranks);

        Integer rankWinner = ranks.get(n-1);
        System.out.println("Rank Winner: " + rankWinner);

        candidates.sort(Candidate::compareTo);

        System.out.println("Winner list: " + candidates);
        System.out.println("Magic number: " + n);

        return candidates.get(n-1).getName();
    }

}