package PrizeDraw;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class LotteryTest {
    private String names = "COLIN,AMANDBA,AMANDAB,CAROL,PauL,JOSEPH";
    private Integer[] weights = {1, 4, 4, 5, 2, 1};
    private String trueWinner = "PauL";
    private int n = 4;

    @Test
    void splitterTest(){
        final String[] splitNames = names.split("\\W");
        assertAll(
                () -> assertEquals( 6, splitNames.length),
                () -> assertEquals( "COLIN", splitNames[0]),
                () -> assertEquals( "JOSEPH", splitNames[5])
        );
    }

    @Test
    void countRankTest(){
        Lottery lottery = new Lottery(names, weights, n);

        Integer rank = lottery.countRank(trueWinner, 2);
        Integer expectedRank = 108;

        assertEquals(expectedRank, rank);
    }

    @Test
    void winnerTest(){
        Lottery lottery = new Lottery(names, weights, n);
        String winner = lottery.getWinner();
        System.out.println(lottery.getRanks());
        assertEquals(trueWinner , winner);
    }

    @Test
    void countRank(){
        Lottery lottery = new Lottery(names, weights, n);

        System.out.println(lottery.countRank("Elijah", 1));
        System.out.println(lottery.countRank("Chloe", 3));
        System.out.println(lottery.countRank("Elizabeth", 5));
        System.out.println(lottery.countRank("Matthew", 5));
        System.out.println(lottery.countRank("Natalie", 3));
        System.out.println(lottery.countRank("Jayden", 6));
    }
}