package PrizeDraw;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RankTest {

    @Test
    void stringComparator(){
        String one = "Abigail";
        String two = "Chloe";
        ArrayList<String> names = new ArrayList<>();
        names.add(one);
        names.add(two);

        System.out.println(one.compareTo(two));
        System.out.println(two.compareTo(one));
        System.out.println(names);
        names.sort(String.CASE_INSENSITIVE_ORDER);
        System.out.println(names);
    }

    @Test
    void test() {
        System.out.println("Fixed Tests");
        String st = "";
        Integer[] we = new Integer[] {4, 2, 1, 4, 3, 1, 2};

        assertEquals("No participants", Rank.nthRank(st, we, 4));

        st = "Addison,Jayden,Sofia,Michael,Andrew,Lily,Benjamin";
        we = new Integer[] {4, 2, 1, 4, 3, 1, 2};
        assertEquals("Not enough participants", Rank.nthRank(st, we, 8));

        st = "Addison,Jayden,Sofia,Michael,Andrew,Lily,Benjamin";
        we = new Integer[] {4, 2, 1, 4, 3, 1, 2};
        assertEquals("Benjamin", Rank.nthRank(st, we, 4));

        st = "Elijah,Chloe,Elizabeth,Matthew,Natalie,Jayden";
        we = new Integer[] {1, 3, 5, 5, 3, 6};
        assertEquals("Matthew", Rank.nthRank(st, we, 2));

        st = "Elijah,Chloe,Elizabeth,Matthew,Natalie,Jayden";
        we = new Integer[] {1, 3, 5, 5, 3, 6};
        assertEquals("Matthew", Rank.nthRank(st, we, 2));
    }
}